﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PickupableObject : MonoBehaviour {

	const int HELD_LAYER = 9;	//YOU SHOULD SET THIS VALUE TO A DEDICATED LAYER THAT DOES NOT COLLIDE WITH THE PLAYER.

	[SerializeField] bool switchLayer = true;	//or set this to false.
	LayerMask layersWhileNotHeld;
	Transform t;
	Rigidbody b;
	RewindOverseer o = null;

	void Start(){
		layersWhileNotHeld = gameObject.layer;
		t = GetComponent<Transform> ();
		b = GetComponent<Rigidbody> ();
	}

	//this needs to be mapped to the OnSetupComplete method of the RewindableObject script.
	public void InitializeWithRewind(ManagerPointer p){
		o = p.GetOverseer ();
	}

	public void Pickup(){
		gameObject.layer = HELD_LAYER;
	}

	public void Drop(){
		gameObject.layer = layersWhileNotHeld;
	}
	
	public bool CanBePickedUp(){
		if (o != null) {
			return !o.IsRewinding ();
		}
		return true;
	}

	public Transform GetTransform(){
		return t;
	}

	public Rigidbody GetRigidbody(){
		return b;
	}

	//should go to the Held Object layer, so that it no longer collides with the player.
}
