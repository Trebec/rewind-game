﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PickupObjectGeneric: MonoBehaviour {

	/// <summary>
	/// Summary: Indicates that this object can be picked up by anyone using the pickup controls script.
	/// 
	/// Usage: Attach this script to any object with a rigidbody that you want to be able to pick up.
	/// 
	/// Credits: Written by Avi Miller. Usage is free, but credit is requested! Enjoy.
	/// </summary>

	const int HELD_LAYER = 9;	//YOU SHOULD SET THIS VALUE TO A DEDICATED LAYER THAT DOES NOT COLLIDE WITH THE PLAYER.

	[SerializeField] bool switchLayer = true;	//or set this to false.
	LayerMask layersWhileNotHeld;
	Transform t;
	Rigidbody b;

	void Start(){
		layersWhileNotHeld = gameObject.layer;
		t = GetComponent<Transform> ();
		b = GetComponent<Rigidbody> ();
	}

	public void Pickup(){
		gameObject.layer = HELD_LAYER;
	}

	public void Drop(){
		gameObject.layer = layersWhileNotHeld;
	}

	public bool CanBePickedUp(){	//you can add your own conditions here
		return true;
	}

	public Transform GetTransform(){
		return t;
	}

	public Rigidbody GetRigidbody(){
		return b;
	}
}
