﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Antigravity : MonoBehaviour {

	Rigidbody b;

	// Use this for initialization
	void Start () {
		b = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 force = Physics.gravity * Time.deltaTime;
		b.AddForce (-force, ForceMode.VelocityChange);
	}
}
