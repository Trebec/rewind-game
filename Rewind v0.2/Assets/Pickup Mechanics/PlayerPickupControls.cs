﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class PlayerPickupControls : MonoBehaviour {

	/// <summary>
	/// Summary: A script for picking up physics objects and carrying them.
	/// 
	/// Description: The behavior implemented here is modeled after the Half-Life 2 and Portal 1 and 2 mechanic of picking
	/// up objects in the environment and carrying them around.
	/// 
	/// Usage: Attach this script to an object with a camera on it, and attach the PickupableObject script to the object
	/// to be picked up. When the player left-clicks while facing the target object, they will pick it up. Depending on
	/// the settings chosen, the player will drop the object when they click again, or when they release the mouse button.
	/// 
	/// Credits: Written by Avi Miller. Usage is free, but credit is requested! Enjoy.
	/// </summary>

	[SerializeField] bool mouseHold = false;		//does the mouse have to be held down to hold the object?
	[SerializeField] float springConstant = 10f;	//proportion of force versus distance, higher is more force
	[SerializeField] float maxHoldForce = 80f;		//maximum acceleration that the player will exert on the object
	[SerializeField] float dampingFactor = 0.999f;	//proportion of force versus speed, higher is more force
	[SerializeField] float holdDistance = 2f;		//the distance from the holder that the object will be held
	[SerializeField] float holdTolerance = 0.5f;	//distance the object can be from its hold point without being yanked in at full force
	[SerializeField] float playerReach = 2.5f;		//how far the player can pick up objects from

	Transform selfTransform;
	Vector3 holdPoint
	{
		get{
			return selfTransform.position + selfTransform.forward * holdDistance;
		}
	}
		
	PickupableObject heldObject = null;

	//initialization
	void Start () {
		selfTransform = transform;	//Save a reference to the object's transform for convenience.
	}

	//update
	void Update () {
		if (mouseHold) {
			MouseHoldMode ();
		} else {
			MouseToggleMode ();
		}
	}

	//behavior requiring the mouse to be held down to keep hold of the object
	void MouseHoldMode(){
		if(Input.GetMouseButtonDown(0)){
			TrySelect ();
		}
		if(Input.GetMouseButton(0)){
			TryHold ();
		}
		if(Input.GetMouseButtonUp(0)){
			TryDeselect ();
		}
	}

	//behavior that holds an object once it is clicked, until the mouse is clicked again
	void MouseToggleMode(){
		if(Input.GetMouseButtonDown(0)){
			if (heldObject == null) {
				TrySelect ();
			} else {
				TryDeselect ();
			}
		}
		TryHold ();
	}

	//--------------------
	//Hold functionality
	//--------------------

	//determines whether the object can/should be moved to the hold point
	void TryHold(){
		if(heldObject == null){
			return;
		}
		if (ObjectDisplacement ().magnitude > playerReach || !heldObject.CanBePickedUp()) {
			TryDeselect ();
		} else {
			Hold ();
		}
	}

	//gotta move the thing to the right position.
	void Hold(){
		ApplyLiftForce ();
		//ApplySpringForce ();
		ApplyAdaptiveForce();
		ApplyDampingForce ();
	}

	//applies a force to negate the force of gravity on the object
	void ApplyLiftForce(){
		if (heldObject.GetRigidbody ().useGravity
			&& heldObject.GetRigidbody ().constraints != RigidbodyConstraints.FreezePositionY
			&& heldObject.GetRigidbody ().constraints != RigidbodyConstraints.FreezeAll) {
			Vector3 force = -Physics.gravity * Time.deltaTime;
			heldObject.GetRigidbody().AddForce (force, ForceMode.VelocityChange);
		}
	}

	/*
	void ApplySpringForce(){	//this makes the force on the object directly proportional to the distance from the target
		Vector3 force = ObjectDisplacement () * springConstant;
		force = force.magnitude > maxHoldForce ? force.normalized * maxHoldForce : force;
		heldObject.GetRigidbody().AddForce (force, ForceMode.VelocityChange);	//choose a good force mode
	}
	*/

	//adds a force as if the object was connected to the hold point by a spring.
	//the spring applies significantly reduced force near the hold point.
	void ApplyAdaptiveForce(){
		Vector3 force = ObjectDisplacement () * springConstant;
		force = force.magnitude > maxHoldForce ? force.normalized * maxHoldForce : force;
		force = force.magnitude < holdTolerance * springConstant ? force.normalized * Mathf.Sqrt(force.magnitude) : force;
		heldObject.GetRigidbody().AddForce (force, ForceMode.VelocityChange);	//choose a good force mode
	}

	//applies force against the object's velocity so it doesn't overshoot
	void ApplyDampingForce(){
		Vector3 force = -heldObject.GetRigidbody().velocity * dampingFactor;
		heldObject.GetRigidbody().AddForce (force, ForceMode.VelocityChange);	//choose a good force mode
	}

	//gives the displacement between the object and the target hold point, using the object as the origin
	Vector3 ObjectDisplacement(){
		return holdPoint - heldObject.GetTransform().position;
	}

	//------------------------
	//Selection functionality
	//------------------------

	//attempts to make an object selection for pickup
	void TrySelect(){
		PickupableObject foundObject = RaycastForPickup ();
		if (foundObject == null || !foundObject.CanBePickedUp())
			return;
		Select (foundObject);
	}

	//chooses an object to pick up
	void Select(PickupableObject p){
		heldObject = p;
		heldObject.Pickup ();
	}

	//tries to let go of an object
	void TryDeselect(){
		if(heldObject != null){
			Deselect ();
		}
	}

	//lets go of the hld object
	void Deselect(){
		heldObject.Drop ();
		heldObject = null;
	}

	//does a raycast along transform.forward and tries to find a PickupableObject on the target.
	PickupableObject RaycastForPickup(){
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit hit;
		PickupableObject p = null;
		if (Physics.Raycast (ray, out hit, playerReach)) {	//do a raycast...
			p = hit.collider.gameObject.GetComponent<PickupableObject> ();
		}
		Debug.Log ("Raycast for pickup. Result: " + p == null ? "null" : "Found target");
		return p;
	}
}
