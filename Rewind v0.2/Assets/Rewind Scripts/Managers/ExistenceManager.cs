﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExistenceManager : RewindManager {

	Timeline<SpawnAndDestroyData> tLine = new Timeline<SpawnAndDestroyData>();
	//GameObject copyObject;

	/*
	public override void Register(GameObject obj){
		//print ("You called?");
		foreach(Component c in obj.GetComponents(typeof(Component))){
			if(c != null)
				print (c);
		}
		base.Register (obj);
	}
	*/

	public override void OnNormalTimeForward(){
		tLine.Save (new SpawnAndDestroyData(obj != null), currentTime);
	}

	public override void OnBeginTimeControl(){
		
	}

	public override void OnContinueTimeControl(){
		
	}

	public override void OnEndTimeControl(){
		
	}
}
