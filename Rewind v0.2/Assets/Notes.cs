﻿/*
If I have a conveyor belt of objects, I just need to have a timeline of one of the objects on the conveyor,
then I can create any number of copies and simply do a time shift on every point in their timeline to get them
to move in sequence. When I do the rewind, if I want it to seem like there are an infinite number of items, just
spawn a new one at the end and give it a shifted copy of the previous item's timeline.

*/