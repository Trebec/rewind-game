﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class TriggerScript : MonoBehaviour {

	[SerializeField] string[] acceptableTags;
	[SerializeField] bool repeatable;
	[SerializeField] UnityEvent onEnter;
	[SerializeField] UnityEvent onStay;
	[SerializeField] UnityEvent onExit;

	bool wasTriggered = false;

	void OnTriggerEnter(Collider collider){
		foreach(string correctTag in acceptableTags){
			if(collider.tag == correctTag){
				Respond ();
				return;
			}
		}
	}

	void OnTriggerStay(Collider collider){
		foreach(string correctTag in acceptableTags){
			if(collider.tag == correctTag){
				ContinueRespond ();
				return;
			}
		}
	}

	void OnTriggerExit(Collider collider){
		foreach(string correctTag in acceptableTags){
			if(collider.tag == correctTag){
				EndRespond ();
				return;
			}
		}
	}

	void Respond(){
		if (!repeatable && wasTriggered) {
			return;
		}
		wasTriggered = true;
		onEnter.Invoke ();
	}

	void ContinueRespond(){
		onStay.Invoke ();
	}

	void EndRespond(){
		onExit.Invoke ();
	}
}
