﻿using UnityEngine;
using System.Collections;

public class Button : ToggleObject {
	
	bool isPressed;

	protected override bool ConditionFunction (){
		return isPressed;
	}

	void OnCollisionEnter(){	//fix it up
		isPressed = true;
	}
}
