﻿using UnityEngine;
using System.Collections;

public class ToggleObject : MonoBehaviour {

	[SerializeField] ToggleObject[] inputSources;
	[SerializeField] bool requireAllSources = true;
	[SerializeField] ToggleObject[] outputTargets;
	[SerializeField] bool defaultIsOn = false;
	bool isOn;

	void Start(){
		isOn = defaultIsOn;
	}

	void Update(){
		UpdateState ();
	}

	void UpdateState(){
		if (inputSources.Length == 0) {
			isOn = ConditionFunction ();
		} else {
			if (requireAllSources) {
				foreach (ToggleObject t in inputSources) {
					if (!t.isOn) {
						isOn = false;
						return;
					}
				}
				isOn = true;
			} else {
				foreach (ToggleObject t in inputSources) {
					if (t.isOn) {
						isOn = true;
						return;
					}
				}
				isOn = false;
			}
		}
	}

	protected virtual bool ConditionFunction(){	//override this for buttons and stuff
		return true;
	}

}
