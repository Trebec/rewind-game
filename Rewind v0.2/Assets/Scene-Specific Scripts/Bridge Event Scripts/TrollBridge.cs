﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class TrollBridge : MonoBehaviour {

	[SerializeField] float force;
	[SerializeField] Vector3 direction;
	[SerializeField] float torque;
	[SerializeField] Vector3 torqueDirection;

	Rigidbody b;
	RewindOverseer o;

	//this function should be called by the RewindableObject script on the same object by mapping
	//this to that script's UnityEvent. That system is AMAAAAAZING and the delegate system is AMAAAAAZING too!
	public void OnSetup(ManagerPointer p){
		o = p.GetOverseer ();
		b = GetComponent<Rigidbody> ();
	}

	public void Trollololol(){
		o.SetTimeControl (true);
		o.AdjustTime (-o.GetCurrentTime());
		o.SetTimeControl (false);
		Debug.Log ("Boop.");

		b.AddForce (direction.normalized * force, ForceMode.Impulse);	//kick it
		b.AddRelativeTorque(torqueDirection.normalized * torque, ForceMode.Impulse);

		//add dialogue here
	}
}
