﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Ram : MonoBehaviour {

	enum MotionStage{
		Standby, Firing, ResetWait, Resetting
	}

	[SerializeField] float oneTimeDelay;
	[SerializeField] float cooldown;
	[SerializeField] float speed;
	[SerializeField] float resetSpeed;
	[SerializeField] float resetDelay;
	[SerializeField] Vector3 axis;

	Rigidbody r;
	MotionStage stage = MotionStage.Standby;
	float remainingCooldown;

	// Use this for initialization
	void Start () {
		r = GetComponent<Rigidbody> ();
		remainingCooldown = cooldown + oneTimeDelay;
	}
	
	// Update is called once per frame
	void Update () {
		remainingCooldown -= Time.deltaTime;
		if(remainingCooldown <= 0f){
			BehaviorSwitch ();
		}
		ResolveBehavior ();
	}

	void BehaviorSwitch(){
		switch (stage) {	//go from the current stage to the next; add any delay
		case MotionStage.Standby:
			stage = MotionStage.Firing;
					break;
		case MotionStage.Firing:
			stage = MotionStage.ResetWait;
			DelayReset ();
			break;
		case MotionStage.ResetWait:
			stage = MotionStage.Resetting;
			break;
		case MotionStage.Resetting:
			stage = MotionStage.Standby;
			Cooldown ();
			break;
		}
	}

	void ResolveBehavior(){
		switch (stage) {
		case MotionStage.Standby:
			break;
		case MotionStage.Firing:
			Launch ();
			break;
		case MotionStage.ResetWait:
			break;
		case MotionStage.Resetting:
			Reset ();
			break;
		}
	}

	void Launch(){
		Debug.Log ("Launch");
		r.AddForce (axis.normalized * speed, ForceMode.VelocityChange);
	}

	void DelayReset(){
		Debug.Log ("DelayReset");
		remainingCooldown = resetDelay;
	}

	void Reset(){
		Debug.Log ("Reset");
		r.AddForce (-axis.normalized * resetSpeed, ForceMode.VelocityChange);
	}

	void Cooldown(){
		Debug.Log ("Cooldown");
		remainingCooldown = cooldown;
	}
}
